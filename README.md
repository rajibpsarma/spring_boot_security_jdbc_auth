# Spring Boot security using JDBC Authentication

It's a demo app that uses a custom login page and JDBC authentication to verify credentials.
The credentials are stored in H2 database.

Also, it uses role based authorization to deny access to certain pages.
Custom access denied page is displayed when a user is not authorized to access that page.

It uses JSP as view. Bootstrap is used to style the pages.

## The steps to be followed are: ##
* git clone https://rajibpsarma@bitbucket.org/rajibpsarma/spring_boot_security_jdbc_auth.git
* cd spring_boot_security_jdbc_auth
* mvn clean spring-boot:run
* Invoke the app using "http://localhost:8080/"
* Enter invalid credentials xyz/xyz, check the error message.
* Enter valid credentials rajib/rajib (having role USER), navigates to home page.
* It can access "Show Users" page, but can not access "Show User's Salaries" and "System Maintenance" pages.
* Logout and login as manager/manager (having role MANAGER).
* It can access "Show Users" and "Show User's Salaries" pages, but can not access "System Maintenance" page.
* Logout and login as admin/admin (having role ADMIN).
* It can access "Show Users" and "System Maintenance" pages, but can not access "Show User's Salaries" page.