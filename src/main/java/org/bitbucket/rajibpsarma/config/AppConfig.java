package org.bitbucket.rajibpsarma.config;

import org.bitbucket.rajibpsarma.auth.UserPrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
/**
 * 
 * @author RSarma
 */
public class AppConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserPrincipalService userDetailsService;
	
	private DaoAuthenticationProvider buildAuthProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(userDetailsService);
		// Use Bcrypt encoding for Passwords
		provider.setPasswordEncoder(new BCryptPasswordEncoder());
		provider.setAuthoritiesMapper(buildAuthMapper());
		return provider;
	}
	
	// It prefixes role with "ROLE_" 
	private GrantedAuthoritiesMapper buildAuthMapper() {
		SimpleAuthorityMapper mapper = new SimpleAuthorityMapper();
		//mapper.setConvertToUpperCase(true);
		//mapper.setDefaultAuthority("USER");
		return mapper;
	}
	
	// JDBC authentication
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(buildAuthProvider());
	}
	
	// Security related configuration
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()	// Allows restricting access
			
			// Authorization rules
			.antMatchers("/").hasRole("USER")
			.antMatchers("/userSalary").hasRole("MANAGER")
			.antMatchers("/system").hasRole("ADMIN")	
			
			// Authentication rules
			.antMatchers("/css/*", "/login").permitAll() // CSS files are allowed, used in login page
			.anyRequest().authenticated()	// All requests need authentication

			.and()
				.formLogin()	// Enable Form based authentication
				.loginPage("/login")	// The login page
				.loginProcessingUrl("/authenticate") // The URL to validate the credentials
				.defaultSuccessUrl("/home",true)
				.permitAll()
			.and()
				.logout().permitAll() // Enable logout functionality, default url "/logout"
			.and()
				// A Custom page for 403 errors
				.exceptionHandling().accessDeniedPage("/accessDenied");
	}
}
