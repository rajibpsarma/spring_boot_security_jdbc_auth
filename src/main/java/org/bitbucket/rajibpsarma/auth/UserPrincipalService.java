package org.bitbucket.rajibpsarma.auth;

import java.util.List;

import org.bitbucket.rajibpsarma.dataaccess.AuthGroup;
import org.bitbucket.rajibpsarma.dataaccess.AuthGroupRepository;
import org.bitbucket.rajibpsarma.dataaccess.User;
import org.bitbucket.rajibpsarma.dataaccess.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * It's used as user DAO.
 * @author RSarma
 */
@Component
public class UserPrincipalService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AuthGroupRepository authGroupRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// Get user
		User user = userRepository.findByUserName(username);
		if(user == null) {
			throw new UsernameNotFoundException("User '" + username + "' is not found");
		}
		// Get roles of the user
		List<AuthGroup> userAuthGroups = authGroupRepository.findAuthGroupsByUserName(username);
		
		
		return new UserPrincipal(user, userAuthGroups);
	}

}
