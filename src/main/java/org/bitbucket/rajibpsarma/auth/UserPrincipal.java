package org.bitbucket.rajibpsarma.auth;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bitbucket.rajibpsarma.dataaccess.AuthGroup;
import org.bitbucket.rajibpsarma.dataaccess.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Store user information (user + roles) which is later encaptulated into Authentication object
 * @author RSarma
 */
public class UserPrincipal implements UserDetails {
	private User user;
	private List<AuthGroup> userAuthGroups;
	
	public UserPrincipal(User user, List<AuthGroup> userAuthGroups) {
		this.user = user;
		this.userAuthGroups = userAuthGroups;
	}
	
	// Returns the roles associated to the user
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		//return Collections.singleton(new SimpleGrantedAuthority("USER"));
		if(this.userAuthGroups.isEmpty()) {
			return Collections.emptySet();
		}
		
		Set<SimpleGrantedAuthority> auth = new HashSet<SimpleGrantedAuthority>();
		this.userAuthGroups.forEach((authGroup) -> {
			auth.add(new SimpleGrantedAuthority(authGroup.getAuthGroup()));
		});
		return auth;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
