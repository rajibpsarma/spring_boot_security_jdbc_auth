package org.bitbucket.rajibpsarma.dataaccess;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USERS")
public class User {
	@Column(name="USER_ID")
	@Id
	private long userId;
	
	@Column(name="USERNAME", nullable = false, unique = true)
	private String userName;
	
	@Column(name="PASSWORD", nullable = false)
	private String password;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}