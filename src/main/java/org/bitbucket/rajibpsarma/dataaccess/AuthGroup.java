package org.bitbucket.rajibpsarma.dataaccess;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AUTH_USER_GROUP")
public class AuthGroup {
	@Column(name="AUTH_USER_GROUP_ID", nullable = false)
	@Id
	private long groupId;
	
	@Column(name="USERNAME", nullable = false)
	private String userName;
	
	@Column(name="AUTH_GROUP", nullable = false)
	private String authGroup;

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAuthGroup() {
		return authGroup;
	}

	public void setAuthGroup(String authGroup) {
		this.authGroup = authGroup;
	}
}
