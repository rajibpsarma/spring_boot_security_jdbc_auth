INSERT INTO USERS (USERNAME, PASSWORD) VALUES
-- user rajib/rajib, password is Bcrypted
('rajib','$2a$10$y1eZM/lxG/Nuj6245xIc6./5gXY9V9c5ibv261/QA5FXr3fbTQH5y'),
-- user manager/manager
('manager','$2a$10$eo9WNdNVbUSyZUzlBGVNCOFudaWnercV.qeQ68BCz8S80stYAf2PS'),
-- user admin/admin
('admin','$2a$10$cuzf/TNfLYDIIQIJ7wKbI.W/jiVtW8JWLudQG69.ZZ0zAWoOH9tFq');

INSERT INTO AUTH_USER_GROUP(USERNAME, AUTH_GROUP) VALUES
('rajib', 'USER'),  -- 'rajib' is an ordinary user
('manager', 'USER'),  -- 'manager' is user + manager
('manager', 'MANAGER'),
('admin', 'USER'),  -- 'admin' is user + administrator
('admin', 'ADMIN');