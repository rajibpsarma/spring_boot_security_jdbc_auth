<!doctype HTML>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Spring Security Demo</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
	</head>
	
	<body>
		<div class="container border rounded bg-light mt-2">
			<div class="row">
				<div class="col-12"><h5 class="mb-0">Spring Security Demo</h5>
				Uses JDBC authentication
				<hr class="border-primary"></div>
			</div>
			<div class="row mb-5">
				<div class="col-12"><b>System Maintenance</b><br>
				Under development
				</div>
			</div>
			<div class="row">
				<div class="col-3"><a href="/home">Home</a></div>
			</div>
		</div>
	</body>
</html>