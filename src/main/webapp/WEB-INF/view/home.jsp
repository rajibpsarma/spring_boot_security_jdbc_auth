<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!doctype HTML>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Spring Security Demo</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
	</head>
	
	<body>
		<div class="container border rounded bg-light mt-2">
			<div class="row">
				<div class="col-12"><h5 class="mb-0">Spring Security Demo</h5>
				Uses JDBC authentication
				<hr class="border-primary"></div>
			</div>
			<div class="row mb-5">
				<div class="col-12">Welcome ! <br>
				Logged in as <b>${userName}</b> ${userRoles}
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<a href="/users">Show Users</a> (All USERs)<br>
					<a href="/userSalary">Show User's Salaries</a> (MANAGERs only)<br>
					<a href="system">System Maintenance</a> (ADMINs only)<br>
					<a href="#" onclick="logout()">Logout</a>
				</div>
			</div>
		</div>
		<form:form method="post" action="/logout" name="logoutForm"></form:form>
		<script>
			function logout() {
				document.logoutForm.submit();
			}
		</script>
	</body>
</html>